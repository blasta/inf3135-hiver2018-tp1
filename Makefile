.PHONY: all tests clean

all: bin/generator

bin/generator: src/generator.c
	mkdir -p bin
	gcc -std=c11 -o bin/generator src/generator.c

bin/pandemic: src/pandemic.c
	mkdir -p bin
	gcc -std=c11 -o bin/pandemic src/pandemic.c


check:
	./tests.sh

clean:
	rm -rf bin/
	rm -rf tests_out/
