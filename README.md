# TP1: `pandemi.c`

## Description

Afin de visualiser la propagation d'un virus il nous est demandé
d'écrire un simulateur capable de montrer l'évolution du virus, jour après jour,
au sein d'une population en fonction d'un scénario de départ.

## Auteur
####INF3135 - HIVER 2018

####Mozart Cadet 
####CADM11078405

## Fonctionnement

Pour compiler le programme vous devrez utilisez les commandes suivante : 


### Générateur de matrice

Le programme `pandemic` permet de générer des cartes de configuration aléatoirement.
Il est très pratique pour tester votre programme.

Usage:
```sh
Usage: pandemic <nombre_de_jour> <matrice>
```

Exemple d'utilisation:
```sh
bin/pandemic 2 < votre_matrice.txt
bin/generator 2 ..................XHX................... 
```

### Générateur de cartes

Le programme `generator` permet de générer des cartes de configuration aléatoirement.
Il est très pratique pour tester votre programme.

Usage:
```sh
Usage: generator <lignes> <colonnes>
```

Exemple d'utilisation:
```sh
make bin/generator
bin/generator 5 10
```

Affiche:
```sh
..X...HX..
Q.XXX...HQ
H..XQ.H...
.XHXXXXX..
...HXXQ.X.
```

## Références



https://stackoverflow.com/

https://www.developpez.com/

https://www.amazon.ca/Jamsas-Programmers-Bible-Ultimate-Programming/dp/1884133258

** + Notes de cours

## État du projet

Indiquez toutes les tâches qui ont été complétés en insérant un `X` entre les
crochets. Si une tâche n'a pas été complétée, expliquez pourquoi.

* [ X ] Le nom du dépôt GitLab est exactement `inf3135-hiver2018-tp1` (Pénalité de
  **50%**).
* [ X ] L'URL du dépôt GitLab est exactement (remplacer `utilisateur` par votre
  nom identifiant GitLab) `https://gitlab.com/utilisateur/inf3135-hiver2018-tp1`
  (Pénalité de **50%**).
* [ X ] Les utilisateurs `kadouche`, `rmTheZ` et `sim590` doivent avoir accès au
  projet en mode *Developer* (Pénalité de **50%**).
* [ X ] Le dépôt GitLab est privé (Pénalité de **50%**).
* [ X ] Le fichier `.gitignore` a été mis à jour.
* [ X ] Le fichier `pandemi.c` a été implémenté.
* [ X ] La cible `bin/pandemic` a été ajoutée dans le Makefile.
* [ * ] Aucun test exécuté avec la commande `make check` n'échoue.
    * les test 11 - 12 et 13 échoue
    * Malheureusement je me suis trop compliqué la tâche en passant par de pointeur et je n'ai
      pas eu le temps de corriger toutes les erreurs
* [ X ] Les sections incomplètes du fichier `README.md` ont été complétées.
