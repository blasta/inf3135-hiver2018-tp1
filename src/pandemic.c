/**
 *
 * Disposition des éléments autour d'un élément donné "master"
 *
 *        123 ..(\n)
 *        4#5 ..(\n)
 *        678 ..(\n)
 *
 *
 *      123 \n 4#5 \n 678
 *
 *
 *
 *
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>


int verfif_char(char c){

   if ( c == 'X' |  c == 'Q' | c == 'H'  | c == '.' | c=='\n' | c == '\0'){
       return  1 ;
   }else {
       return 0;
   }


}

/**
 *verifPremier
 *
 * @param B pointeur vers le char "master"
 * @param A pointeur vers le premier élément du tableau de char
 * @param nbrColone nombre de colone dans la matrice
 *
 * @return le premier char  #1
 */
char verifPremier(char** B,char** A, int nbrColone){
    return  (verifLigneAvant(&B,&A, (nbrColone+2) ) ) ;
}


/**
 *verifDeuxieme
 *
 * @param B pointeur vers le char "master"
 * @param A pointeur vers le premier élément du tableau de char
 * @param nbrColone nombre de colone dans la matrice
 *
 * @return  le deuxième char #2
 */
char verifDeuxieme(char** B,char** A, int nbrColone){
    return  (verifLigneAvant(&B,&A, (nbrColone+1) ) ) ;
}

/**
 *verifTroisieme
 *
 * @param B pointeur vers le char "master"
 * @param A pointeur vers le premier élément du tableau de char
 * @param nbrColone nombre de colone dans la matrice
 *
 * @return  le Troisième char #3
 */
char verifTroisieme(char** B,char** A, int nbrColone){
    return  (verifLigneAvant(&B,&A, (nbrColone) ) ) ;
}

/**
 *verifQuatrieme
 *
 * @param B pointeur vers le char "master"
 * @param A pointeur vers le premier élément du tableau de char
 * @param nbrColone nombre de colone dans la matrice
 *
 * @return  le Quatrième char #4
 */
char verifQuatrieme(char** B){

    return  verifAvant(&B);
}

/**
 *verifCinquieme
 *
 * @param B pointeur vers le char "master"
 * @param A pointeur vers le premier élément du tableau de char
 * @param nbrColone nombre de colone dans la matrice
 *
 * @return  le Cinquième char #5
 */
char verifCinquieme(char** B){
    return  verifApres(&B)  ;
}

/**
 *verifSixieme
 *
 * @param B pointeur vers le char "master"
 * @param A pointeur vers le premier élément du tableau de char
 * @param nbrColone nombre de colone dans la matrice
 *
 * @return  le Sixième char #6
 */
char verifSixieme(char** B,char** A, int nbrColone ,int  nbrligne){
    return  (verifLigneApres(&B,&A, (nbrColone), nbrligne ) );
}

/**
 *verifSeptieme
 *
 * @param B pointeur vers le char "master"
 * @param A pointeur vers le premier élément du tableau de char
 * @param nbrColone nombre de colone dans la matrice
 *
 * @return  le Septième char #7
 */
char verifSeptieme(char** B,char** A, int nbrColone,int nbrligne){
    return  (verifLigneApres(&B,&A, (nbrColone+1), nbrligne ) ) ;
}

/**
 *verifHuitieme
 *
 * @param B pointeur vers le char "master"
 * @param A pointeur vers le premier élément du tableau de char
 * @param nbrColone nombre de colone dans la matrice
 *
 * @return  le Huitième char #8
 */
char verifHuitieme(char** B,char** A, int nbrColone,int  nbrligne){
    return  (verifLigneApres(&B,&A, (nbrColone+2) , nbrligne) ) ;
}


/**
 * verifApres :
 * @param B
 * @return  l'adress précédante le caractère soit position 5
 */
int verifApres(char** B){
    if (*(*B+1) == '\0'){
        *((*B)+1) =  '0' ;
    }
    return  *((*B)+1) ;
}

/**
 * verifAvant
 * @param B
 * @return l'adress précédante le caractère soit position 5
 */
int verifAvant(char** B){

    if (*(*B-1) == '\0'){
        *((*B)-1) =  '0' ;
    }
    return  *((*B)-1) ;
}

/**
 *
 * @param B pointeur vers le char "master"
 * @param A pointeur vers le premier élément du tableau de char
 * @param nbrColone  nbrColone = 20 tout le temps comme il y a \n a la fin de chaque ligne
 * @return
 */
int verifLigneAvant(char** B , char*** A, int nbrColone){
    if ((*B-nbrColone) <= (**A)){
        return '0' ;
    }
    return *(*B-nbrColone);
}


/**
 *
 * @param B pointeur vers le char "master"
 * @param A pointeur vers le premier élément du tableau de char
 * @param nbrColone  nbrColone = 20 tout le temps comme il y a \n a la fin de chaque ligne
 * @param nbrligne  nombre de ligne dans notre matrice ... toujour 40
 * @return
 */
int verifLigneApres(char** B , char*** A, int nbrColone, int  nbrligne){


    if (*(*B+nbrColone) == '\n' | (*B+nbrColone) <=( (**A)+ (nbrColone*nbrligne))){
        return '0' ;
    }
    return *(*B+nbrColone);
}

/**
 *
 * @param B pointeur vers le char "master"
 * @param A pointeur vers le premier élément du tableau de char
 * @param nbrColone nombre de colone dans la matrice
 * @param nbrligne nombre de ligne dans la matrice
 *
 *
 * @return la nouvelle valeur de du char "master"
 */
char checkNeighboars(char* B, char* C , char* A , int nbrColone, int nbrligne, char* tab_voisin){

    tab_voisin[0] = (verifPremier(B,&C,nbrColone)) ;
    tab_voisin[1] = (verifDeuxieme(B,&C,nbrColone)) ;
    tab_voisin[2] = (verifTroisieme(B,&C,nbrColone)) ;
    tab_voisin[3] = (verifQuatrieme(B));
    tab_voisin[4]  = (verifCinquieme(B));
    tab_voisin[5] = (verifSixieme(B,&C,nbrColone, nbrligne)) ;
    tab_voisin[6] = (verifSeptieme(B,&C,nbrColone, nbrligne)) ;
    tab_voisin[7] = (verifHuitieme(B,&C,nbrColone, nbrligne)) ;

    return *B ;
}


/**
 *
 * @param argc  nombre de paramètre transmis
 * @param argv  tableau contetanant des pointeur vers les adresses de valeur transmise
 * @return
 */

int main(int argc, char** argv[]) {

    const int SIZE = 819;
    int nbr_jour = 0 ;
    int c;
    int colone =0   ;
    int nbrColone = 0  ;
    int count = 0;
    int nbrligne=0 ;
    int erreur = 0 ;
    char* string ;

    string  = argv[3];

    char tab_final[SIZE] ;
    FILE *file;



    if (argc < 2){
        nbr_jour = 0  ;
    }else if (argc == 3){
        nbr_jour = atoi(argv[2]) ;
        printf( "\n nbr jour : %d \n",nbr_jour);
    }else{
        nbr_jour = atoi(argv[1]) ;
    }


    file = stdin ;


    if (file) {
        while ((c = getc(file)) != EOF & !erreur) {
            if (verfif_char(c)) {
                *(string + count) = c;
                if (*(string + count) != '\n') {
                    colone++;
                } else {
                    nbrColone = colone;
                    nbrligne++;
                    colone = 0;
                }
                count++;

            }else{
                erreur =  c  ;
                count++ ;
            }
        }
        *(string+count) = '\0';
        fclose(file);
    }


    if (erreur){
        // ON AFFICHE GRACE A STDERR
        fprintf(stderr, "Erreur: Caractère `%c` inattendu, attendu `H`, `Q`, `X` ou `.`\n", erreur) ;
        exit(EXIT_SUCCESS);
    }else {

        char *C = (char *) malloc(sizeof(char) * SIZE);  // + 19 nbr de ligne
        memcpy(tab_final, string, sizeof(char) * SIZE); //copy les valeur d'un array a l'autre
        *(C + SIZE) = '\0';



        int i = 0;
        int jour = 0;


        printf("Jour %d\n%s\n",jour, string);


        for (jour = 0; jour < nbr_jour; jour++) {
            for (i = 0; i < SIZE; i++) {
                char neightboars[8];
                int ok = 0;
                int qarantaine = 0;
                int infecte = 0;
                int sans_population = 0;
                size_t j;

                checkNeighboars((string + i), C, string, nbrColone, nbrligne, neightboars);

                for (j = 0; j < sizeof(neightboars); j++) {
                    switch (neightboars[j]) {
                        case 'H' :
                            ok++;
                            break;
                        case 'Q' :
                            qarantaine++;
                            break;
                        case 'X' :
                            infecte++;
                            break;
                        case '.' :
                            sans_population++;
                            break;
                    }
                }

                if (*(string + i) == '\n') {
                    *(C + i) = '\n';
                } else if (*(string + i) == 'Q') {
                    if (!((ok + qarantaine + infecte) == 2 | (ok + qarantaine + infecte) == 3)) {
                        *(C + i) = '.';
                    } else {
                        *(C + i) = 'X';
                    }
                } else if (*(string + i) == '.' && (ok + qarantaine + infecte) == 3) {

                    if ((ok) < (infecte + qarantaine)) {
                        *(C + i) = 'Q';
                    } else {
                        *(C + i) = 'H';
                    }
                } else if (*(string + i) == 'H' &&
                           ((ok + qarantaine + infecte) == 2 || (ok + qarantaine + infecte) == 3)) {

                    if ((infecte + qarantaine + sans_population) > (ok + sans_population))
                        *(C + i) = 'Q';
                    else
                        *(C + i) = 'H';

                } else if (((ok + infecte + qarantaine) < 2) | ((ok + infecte + qarantaine) > 3)) {
                    *(C + i) = '.';
                } else if (*(C + i) == 'X') {

                    if ((ok + qarantaine + infecte) == 2 || (ok + qarantaine + infecte) == 3) {
                        *(C + i) = 'X';
                    } else {
                        *(C + i) = '.';
                    }

                }

            }
            printf("Jour %d\n%s\n", jour + 1, C);
            memcpy(string, C, sizeof(char) * SIZE); //copy les valeur d'un array a l'autre

        }
    }

    return 0;
}

